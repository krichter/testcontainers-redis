package de.richtercloud.testcontainers.redis;

import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.FixedHostPortGenericContainer;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.HostPortWaitStrategy;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.google.common.cache.Cache;
import redis.clients.jedis.Jedis;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@Testcontainers
public class TestcontainersRedisIT {
    private static final String KEY = "events/city/rome";

    @Rule
    private GenericContainer redis = new FixedHostPortGenericContainer("redis:5");

    @BeforeEach
    public void setUp() {
        redis.start();
    }

    @AfterEach
    public void tearDown() {
        redis.stop();
    }

    @Test
    @Order(1)
    public void testcontainersRedisTest() {
        Jedis jedis = new Jedis(redis.getContainerIpAddress(), redis.getMappedPort(6379));
        String result = "32,15,223,828";
        jedis.set(KEY, result);
        String cachedResponse = jedis.get(KEY);
        assertEquals(cachedResponse, result);
    }

    @Test
    @Order(2)
    public void testcontainersRedisTest2() {
        Jedis jedis = new Jedis(redis.getContainerIpAddress(), redis.getMappedPort(6379));
        String cachedResponse = jedis.get(KEY);
        assertNull(cachedResponse);
    }
}
